# control

A simple control plane for the obfsvpn node.
It's part of the [obfsvpn-server](/images/obfsvpn/Dockerfile) docker image.

Control is intended to be used for communication between different services, such as the gateway and bridge distribution service  [Menshen](https://0xacab.org/leap/menshen).

It exposes the bridge configuration and offers to flag a node as down for maintenance.

The endpoints listening to GET requests are:
* `/bridge`: returns bridge config as IP, port, certificate, location and maintenance state
* `/enable`: flags the bridge as working
* `/disable`: flags the bridge as being in maintenance

A couple of environment variables need to be set to run control:

`CONTROL_PORT`: port control is listening to
`OBFSVPN_STATE`: directory of private and public key, certifcate, bridge-line file
`OBFSVPN_LOCATION`: key representing a location of the OpenVPN gateway the bridge is pointing to. It will be used by [Menshen](https://0xacab.org/leap/menshen) to filter requests to the Menshen API for a bridge location 
`OBFS4_IP`: public IP of the bridge, exposed in the Menshen API
`OBFS4_PORT`: port of the bridge, exposed in the Menshen API


