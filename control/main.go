// A simple control plane for the openvpn node
package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

const (
	CONTROL  = "CONTROL_PORT"
	STATE    = "OBFSVPN_STATE"
	LOCATION = "OBFSVPN_LOCATION"
	IP       = "OBFS4_IP"
	PORT     = "OBFS4_PORT"
)

type bridgeInfo struct {
	IPAddr     string `json:"ip_addr"`
	BridgeType string `json:"type"`
	Healthy    bool   `json:"healthy"`
	Location   string `json:"location"`
	Options    struct {
		Cert    string `json:"cert"`
		IatMode int    `json:"iat-mode"`
	} `json:"options"`
	Port int `json:"port"`
}

var (
	StateDir = os.Getenv(STATE)
	Healthy  = true
)

func main() {
	controlPort := os.Getenv(CONTROL)
	if controlPort == "" {
		fmt.Println(CONTROL, "not set")
		return
	}
	if StateDir == "" {
		StateDir = "/etc/obfsvpn/"
	}
	log.SetFormatter(&log.TextFormatter{})

	e := echo.New()
	e.GET("/bridge", getBridgeInfo)
	e.GET("/disable", disableBridge)
	e.GET("/enable", enableBridge)
	e.Logger.Fatal(e.Start(":" + controlPort))
}

func getLocation() string {
	l := os.Getenv(LOCATION)
	if l == "" {
		l = "unknown"
	}
	return l
}

type cmdResult struct {
	OK bool `json:"ok"`
}

func disableBridge(c echo.Context) error {
	Healthy = false
	return c.JSON(http.StatusOK, &cmdResult{true})
}

func enableBridge(c echo.Context) error {
	Healthy = true
	return c.JSON(http.StatusOK, &cmdResult{true})
}

func getBridgeInfo(c echo.Context) error {
	port, err := strconv.Atoi(os.Getenv(PORT))
	if err != nil {
		return fmt.Errorf("error parsing port: %v", err)
	}
	b := &bridgeInfo{
		// TODO get kcp, hop type here
		BridgeType: "obfs4",
		Location:   getLocation(),
		IPAddr:     os.Getenv(IP),
		Port:       port,
		Healthy:    Healthy,
	}

	f, err := os.Open(filepath.Join(StateDir, "obfs4_bridgeline.txt"))
	if err != nil {
		log.Warning(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "could not open bridgeline file")
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var cert string
	var iatMode int

	// The line we're targeting has the following format:
	// Bridge obfs4 <IP ADDRESS>:<PORT> <FINGERPRINT> cert=87+zEbuoIIAWAPNPJ5Ci30hiFSroMjwESD1QBJGAyc9HTkCpKKJkF/Gh8kzK0Io9iI9Zbw iat-mode=1

	for scanner.Scan() {
		line := string(scanner.Text())
		if !strings.HasPrefix(line, "Bridge") {
			continue
		}
		for _, chunk := range strings.Split(line, " ") {
			if strings.HasPrefix(chunk, "cert=") {
				cert = chunk[len("cert="):]
				b.Options.Cert = cert
			}
			if strings.HasPrefix(chunk, "iat-mode=") {
				iatStr := chunk[len("iat-mode="):]
				iatMode, err = strconv.Atoi(iatStr)
				if err != nil {
					return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error reading bridgeline file: %v", err))
				}
				b.Options.IatMode = iatMode
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Warning(err)
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Sprintf("error reading bridgeline file: %v", err))
	}

	return c.JSON(http.StatusOK, b)
}
