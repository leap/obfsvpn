package obfsvpn

import (
	"context"
	"testing"
)

const cert = "8nuAbPJwFrKc/29KcCfL5LBuEWxQrjBASYXdUbwcm9d9pKseGK4r2Tg47e23+t6WghxGGw"

func TestDial(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("Dial paniced")
		}
	}()

	dialer, err := NewDialerFromCert(cert)
	if err != nil {
		t.Errorf("Error creating client from cert: %v", err)
	}

	_, err = dialer.Dial("tcp", "127.0.0.1:4430")
	if err == nil {
		t.Errorf("No Error in d.Dial when trying invalid address: %v", err)
	}
}

func TestDialContext(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Error("DialContext paniced")
		}
	}()

	dialer, err := NewDialerFromCert(cert)
	if err != nil {
		t.Errorf("Error creating client from cert: %v", err)
	}

	_, err = dialer.DialContext(context.Background(), "tcp", "127.0.0.1:4430")
	if err == nil {
		t.Errorf("No Error in d.Dial when trying invalid address: %v", err)
	}
}
