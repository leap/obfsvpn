// The server command creates an obfsvpn bridge which listens for obfs4 connections and forwards them to an openvpn server
package main

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"

	"0xacab.org/leap/obfsvpn/obfsvpn"
	"0xacab.org/leap/obfsvpn/server"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	addr                 = "addr"
	cfgFile              = "config"
	genkey               = "genkey"
	hop                  = "hop"
	udp                  = "udp"
	listen               = "listen"
	quic                 = "quic"
	quicTLSCertFile      = "quic-tls-cert-file"
	quicTLSKeyFile       = "quic-tls-key-file"
	kcp                  = "kcp"
	kcpSendWindowSize    = "kcp-send-window-size"
	kcpReceiveWindowSize = "kcp-receive-window-size"
	kcpReadBuffer        = "kcp-read-buffer"
	//nolint:gosec // gosec somehow thinks this next line is Potential hardcoded credentials 🤷
	kcpWriteBuffer        = "kcp-write-buffer"
	kcpNoDelay            = "kcp-no-delay"
	kcpInterval           = "kcp-interval"
	kcpResend             = "kcp-resend"
	kcpDisableFlowControl = "kcp-disable-flow-control"
	kcpMTU                = "kcp-mtu"
	persist               = "persist"
	port                  = "port"
	portSeed              = "port-seed"
	portCount             = "port-count"
	minHopPort            = "min-hop-port"
	maxHopPort            = "max-hop-port"
	stateDir              = "state"
	remote                = "remote"
	verbose               = "v"
)

var (
	defaultStateDir       = "/etc/obfsvpn"
	defaultConfigFile     = "obfs4_state.json"
	defaultConfigFilePath = filepath.Join(defaultStateDir, defaultConfigFile)
)

var ErrObfs4DataAlreadyExists = errors.New("obfs4 data already exists")

type config struct {
	addr                  string
	cfgFile               string
	port                  int
	hop                   bool
	udp                   bool
	quic                  bool
	quicTLSCertFile       string
	quicTLSKeyFile        string
	kcp                   bool
	kcpSendWindowSize     int
	kcpReceiveWindowSize  int
	kcpReadBuffer         int
	kcpWriteBuffer        int
	kcpNoDelay            bool
	kcpInterval           int
	kcpResend             int
	kcpDisableFlowControl bool
	kcpMTU                int
	portSeed              int64
	portCount             uint
	minHopPort            uint
	maxHopPort            uint
	stateDir              string
	remote                string
	verbose               bool
}

func newConfigFromViper(logger *log.Logger) *config {
	var addrValue string
	var portValue int
	var err error

	listen := viper.GetString(listen)
	if listen != "" {
		parts := strings.Split(listen, ":")
		if len(parts) != 2 {
			log.Fatal("--listen has wrong format, expected ip:port")
		}
		addrValue = parts[0]
		portValue, err = strconv.Atoi(parts[1])
		if err != nil {
			log.Fatalf("wrong port: %v", parts[1])
		}
	} else {
		addrValue = viper.GetString(addr)
		portValue = viper.GetInt(port)
	}

	cfg := &config{
		addr:                  addrValue,
		cfgFile:               viper.GetString(cfgFile),
		port:                  portValue,
		hop:                   viper.GetBool(hop),
		udp:                   viper.GetBool(udp),
		quic:                  viper.GetBool(quic),
		quicTLSCertFile:       viper.GetString(quicTLSCertFile),
		quicTLSKeyFile:        viper.GetString(quicTLSKeyFile),
		kcp:                   viper.GetBool(kcp),
		kcpSendWindowSize:     viper.GetInt(kcpSendWindowSize),
		kcpReceiveWindowSize:  viper.GetInt(kcpReceiveWindowSize),
		kcpReadBuffer:         viper.GetInt(kcpReadBuffer),
		kcpWriteBuffer:        viper.GetInt(kcpWriteBuffer),
		kcpNoDelay:            viper.GetBool(kcpNoDelay),
		kcpInterval:           viper.GetInt(kcpInterval),
		kcpResend:             viper.GetInt(kcpResend),
		kcpDisableFlowControl: viper.GetBool(kcpDisableFlowControl),
		kcpMTU:                viper.GetInt(kcpMTU),

		portSeed:   viper.GetInt64(portSeed),
		portCount:  viper.GetUint(portCount),
		minHopPort: viper.GetUint(minHopPort),
		maxHopPort: viper.GetUint(maxHopPort),
		stateDir:   getStateDir(),
		remote:     viper.GetString(remote),
		verbose:    viper.GetBool(verbose),
	}

	// Sanity check on the configuration
	if cfg.remote == "" {
		logger.Fatal("must specify ", remote)
	}
	return cfg
}

func getObfs4ConfigFromFile(cfgFile string) (*server.Obfs4Config, error) {
	fd, err := os.Open(cfgFile) //#nosec G304
	if err != nil {
		return nil, err
	}
	defer fd.Close()

	var obfs4Config server.Obfs4Config
	err = json.NewDecoder(fd).Decode(&obfs4Config)
	if err != nil {
		return nil, err
	}
	return &obfs4Config, nil
}

func getStateDir() string {
	stateDir := viper.GetString(stateDir)
	if stateDir == "" {
		if viper.GetBool(persist) {
			// we use a state folder in the root dir by default if the persist option is set
			// (useful for testing)
			stateDir = "state"
		} else {
			// Inside a container, we want to generate ephemeral keys (unless we pass the --persist flag).
			// The container should then not mount /etc/obfsvpn
			stateDir = defaultStateDir
		}
	}
	return stateDir
}

// genKey generates key material. returns the folder where the keys are written, and any error.
func genKey(logger *log.Logger) (string, error) {
	stateDir := getStateDir()
	stateFile := filepath.Join(stateDir, "obfs4_state.json")
	if _, err := os.Stat(stateFile); !errors.Is(err, os.ErrNotExist) {
		return stateDir, fmt.Errorf("%w: %s", ErrObfs4DataAlreadyExists, stateFile)
	}
	// Try to create the state dir if it does not exist, otherwise
	// obfs4 will fail to create the state file.
	if err := os.MkdirAll(stateDir, os.ModePerm); err != nil {
		log.Fatalf("Error creating directory: %v", err)
	}

	logger.Println("Generating key material")
	if err := obfsvpn.NewServerState(stateDir); err != nil {
		return "", err
	}
	return stateDir, nil
}

func getTLSCertFromFile(certFilepath, keyFilepath string) (*tls.Certificate, error) {
	cert, err := tls.LoadX509KeyPair(certFilepath, keyFilepath)

	if err != nil {
		return nil, fmt.Errorf("Failed to load Cert from filepaths: %w", err)
	}

	return &cert, nil
}

func main() {
	// Setup logging.
	logger := log.New(os.Stderr, "", log.LstdFlags)
	debug := log.New(io.Discard, "DEBUG ", log.LstdFlags)

	// Setup command line flags.

	flag.String(addr, "[::1]", "The IP address to listen on for client connections in normal mode [no port]")
	flag.String(port, "4430", "The port to listen on for client connections in normal mode")
	flag.String(listen, "", "The IP:Port tuple address where to listen in normal mode")
	flag.String(cfgFile, "", "The JSON config file to load")

	flag.Bool(genkey, false, "Generate key material")
	flag.Bool(hop, false, "Use hopping PT")
	flag.Bool(quic, false, "Use QUIC for obfs4 transport layer")
	flag.Bool(kcp, false, "Use KCP (UDP) for obfs4 transport layer")
	flag.Bool(udp, false, "Use UDP for openvpn layer")

	flag.Bool(persist, false, "Persist the bridge state")

	flag.String(quicTLSCertFile, "", "Filepath to a TLS cert for QUIC connections. Only necessary for QUIC mode.")
	flag.String(quicTLSKeyFile, "", "Filepath to a TLS key for QUIC connections. Only necessary for QUIC mode.")

	flag.Int(kcpSendWindowSize, obfsvpn.DefaultKCPSendWindowSize, "KCP SendWindowSize")
	flag.Int(kcpReceiveWindowSize, obfsvpn.DefaultKCPReceiveWindowSize, "KCP ReceiveWindowSize")
	flag.Int(kcpReadBuffer, obfsvpn.DefaultKCPReadBuffer, "KCP ReadBuffer")
	flag.Int(kcpWriteBuffer, obfsvpn.DefaultKCPWriteBuffer, "KCP WriteBuffer")
	flag.Bool(kcpNoDelay, obfsvpn.DefaultNoDelay, "KCP NoDelay")
	flag.Int(kcpInterval, obfsvpn.DefaultInterval, "KCP Interval")
	flag.Int(kcpResend, obfsvpn.DefaultResend, "KCP Resend")
	flag.Bool(kcpDisableFlowControl, obfsvpn.DefaultDisableFlowControl, "KCP DisableFlowControl")
	flag.Int(kcpMTU, obfsvpn.DefaultMTU, "KCP MTU")
	flag.Int64(portSeed, 1, "The seed to use for generating random ports for port hopping")
	flag.Uint(portCount, 100, "The number of random ports to listen on for port hopping")
	flag.Uint(minHopPort, obfsvpn.MinHopPort, "The lower limit of the available port range for port hopping")
	flag.Uint(maxHopPort, obfsvpn.MaxHopPort, "The upper limit of the available port range for port hopping")
	flag.String(stateDir, "", "A directory in which to store bridge state")

	flag.Bool(verbose, false, "Enable verbose logging")
	flag.String(remote, "", "The remote address to connect to, usually an OpenVPN endpoint")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	err := viper.BindPFlags(pflag.CommandLine)
	if err != nil {
		log.Fatalf("Error calling BindPFlags: %v", err)
	}

	viper.AddConfigPath(".")
	viper.SetConfigName("obfsvpn")
	viper.SetConfigType("yaml")
	err = viper.ReadInConfig()
	if err != nil {
		log.Printf("Error calling ReadInConfig: %v", err)
	}

	viper.SetEnvPrefix("OBFSVPN")

	//nolint:ineffassign,staticcheck // It's annoying to write a if err != nil block for every single one of these 🤷
	{
		// AutomaticEnv doesn't do well with dashes
		err = viper.BindEnv(portSeed, "OBFSVPN_PORT_SEED")
		err = viper.BindEnv(portCount, "OBFSVPN_PORT_COUNT")
		err = viper.BindEnv(minHopPort, "OBFSVPN_MIN_HOP_PORT")
		err = viper.BindEnv(maxHopPort, "OBFSVPN_MAX_HOP_PORT")
	}
	if err != nil {
		log.Fatalf("Error calling BindEnv: %v", err)
	}

	viper.AutomaticEnv()
	justGenKey := viper.GetBool(genkey)
	persist := viper.GetBool(persist)
	if justGenKey || !persist {
		stateDir, err := genKey(logger)
		if err != nil {
			if justGenKey || !errors.Is(err, ErrObfs4DataAlreadyExists) {
				logger.Fatalf("error while generating key: %v", err)
			}
		}
		logger.Println("Done. Key material written to:", stateDir)
		if justGenKey {
			os.Exit(1)
		}
	}

	cfg := newConfigFromViper(logger)

	// Configure logging.
	if cfg.verbose {
		debug.SetOutput(os.Stderr)
	}

	if (cfg.maxHopPort - cfg.minHopPort) < cfg.portCount {
		logger.Fatalf("configured port hopping boundaries (range of %d ports) cannot be smaller than port count %d.", (cfg.maxHopPort - cfg.minHopPort), cfg.portCount)
	}

	if cfg.cfgFile == "" {
		stateDir := getStateDir()
		if stateDir != "" {
			cfg.cfgFile = filepath.Join(stateDir, defaultConfigFile)
		} else {
			cfg.cfgFile = defaultConfigFilePath
		}
	}

	logger.Println("Using obfs4 config file:", cfg.cfgFile)
	obfs4Config, err := getObfs4ConfigFromFile(cfg.cfgFile)
	if err != nil {
		log.Fatal(err)
	}

	kcpConfig := obfsvpn.KCPConfig{
		Enabled:            cfg.kcp,
		ReceiveWindowSize:  cfg.kcpReceiveWindowSize,
		SendWindowSize:     cfg.kcpSendWindowSize,
		WriteBuffer:        cfg.kcpWriteBuffer,
		ReadBuffer:         cfg.kcpReadBuffer,
		NoDelay:            cfg.kcpNoDelay,
		Interval:           cfg.kcpInterval,
		Resend:             cfg.kcpResend,
		DisableFlowControl: cfg.kcpDisableFlowControl,
		MTU:                cfg.kcpMTU,
	}

	quicConfig := obfsvpn.QUICConfig{
		Enabled: cfg.quic,
	}

	if quicConfig.Enabled {
		cert, err := getTLSCertFromFile(cfg.quicTLSCertFile, cfg.quicTLSKeyFile)
		if err != nil {
			log.Fatalf("Could not load TLS cert from file: %v", err)
		}
		quicConfig.TLSCert = cert
	}

	serverCfg := server.ServerConfig{
		Obfs4Config: *obfs4Config,
		StateDir:    cfg.stateDir,
		PortSeed:    cfg.portSeed,
		PortCount:   cfg.portCount,
		MinHopPort:  cfg.minHopPort,
		MaxHopPort:  cfg.maxHopPort,
		OpenvpnAddr: cfg.remote,
		KCPConfig:   kcpConfig,
		QUICConfig:  quicConfig,
	}
	serverFactory := server.NewTCPServer
	serverCfg.Obfs4ListenIP = cfg.addr
	serverCfg.Obfs4ListenPort = cfg.port
	serverCfg.HoppingEnabled = cfg.hop
	if cfg.udp || cfg.hop {
		serverFactory = server.NewUDPServer
	}

	debug.Printf("kcp: %v, hop: %v, udp: %v, quic: %v", cfg.kcp, cfg.hop, cfg.udp, cfg.quic)

	// Setup graceful shutdown.
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)

	server := serverFactory(ctx, stop, serverCfg, logger, debug)
	if err = server.Start(); err != nil {
		logger.Fatalf("Error running obfsvpn server: %v", err)
	}

	<-ctx.Done()
	// Stop releases the signal handling and falls back to the default behavior,
	// so sending another interrupt will immediately terminate.
	logger.Printf("shutting down…")
	server.Stop()
}
