package main

import (
	"context"
	"flag"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"

	"0xacab.org/leap/obfsvpn/client"
	"0xacab.org/leap/obfsvpn/obfsvpn"
)

func main() {
	// Setup logging.
	logger := log.New(os.Stderr, "", log.LstdFlags)
	debug := log.New(io.Discard, "DEBUG ", log.LstdFlags)

	// setup command line flags.
	var (
		hoppingPT             bool
		hopJitter             uint = 5
		quic                  bool
		kcp                   bool
		kcpSendWindowSize     int   = obfsvpn.DefaultKCPSendWindowSize
		kcpReceiveWindowSize  int   = obfsvpn.DefaultKCPReceiveWindowSize
		kcpReadBuffer         int   = obfsvpn.DefaultKCPReadBuffer
		kcpWriteBuffer        int   = obfsvpn.DefaultKCPWriteBuffer
		kcpNoDelay            bool  = obfsvpn.DefaultNoDelay
		kcpInterval           int   = obfsvpn.DefaultInterval
		kcpResend             int   = obfsvpn.DefaultResend
		kcpDisableFlowControl       = obfsvpn.DefaultDisableFlowControl
		kcpMTU                      = obfsvpn.DefaultMTU
		minHopSeconds         uint  = 5
		portSeed              int64 = 1
		portCount             uint  = 100
		minHopPort            uint  = obfsvpn.MinHopPort
		maxHopPort            uint  = obfsvpn.MaxHopPort
		proxyPort             string
		proxyHost             string
		obfs4Certs            string
		obfs4Remotes          string
		obfs4RemotePort       string
		verbose               bool
	)

	proxyPort = "8080"
	proxyHost = "127.0.0.1"

	flags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flags.BoolVar(&quic, "quic", quic, "Enable QUIC mode")
	flags.BoolVar(&kcp, "kcp", kcp, "Enable KCP mode")
	flags.IntVar(&kcpSendWindowSize, "kcp-send-window-size", kcpSendWindowSize, "KCP SendWindowSize")
	flags.IntVar(&kcpReceiveWindowSize, "kcp-receive-window-size", kcpReceiveWindowSize, "KCP ReceiveWindowSize")
	flags.IntVar(&kcpReadBuffer, "kcp-read-buffer", kcpReadBuffer, "KCP ReadBuffer")
	flags.IntVar(&kcpWriteBuffer, "kcp-write-buffer", kcpWriteBuffer, "KCP WriteBuffer")
	flags.BoolVar(&kcpNoDelay, "kcp-no-delay", kcpNoDelay, "KCP NoDelay")
	flags.IntVar(&kcpInterval, "kcp-interval", kcpInterval, "KCP Interval")
	flags.IntVar(&kcpResend, "kcp-resend", kcpResend, "KCP Resend")
	flags.BoolVar(&kcpDisableFlowControl, "kcp-disable-flow-control", kcpDisableFlowControl, "KCP DisableFlowControl")
	flags.IntVar(&kcpMTU, "kcp-mtu", kcpMTU, "KCP MTU")
	flags.BoolVar(&verbose, "v", verbose, "Enable verbose logging")
	flags.BoolVar(&hoppingPT, "h", hoppingPT, "Connect with openvpn over udp in hopping mode")
	flags.StringVar(&obfs4Certs, "c", obfs4Certs, "The remote obfs4 certificates separated by commas. If hopping is not enabled only the first cert will be used")
	flags.StringVar(&obfs4Remotes, "r", obfs4Remotes, "The remote obfs4 endpoint ips (no port) separated by commas. If hopping is not enabled only the first cert will be used")
	flags.StringVar(&obfs4RemotePort, "rp", obfs4RemotePort, "The remote obfs4 endpoint port to use. Only applicable to NON-hopping")
	flags.UintVar(&portCount, "pc", portCount, "The number of ports to try for each remote. Only applicable to hopping")
	flags.UintVar(&minHopPort, "min-port", minHopPort, "The lower limit of the port range used for port hopping")
	flags.UintVar(&maxHopPort, "max-port", maxHopPort, "The upper limit of the port range used for port hopping")
	flags.Int64Var(&portSeed, "ps", portSeed, "The random seed to generate ports from. Only applicable to hopping")
	flags.UintVar(&minHopSeconds, "m", minHopSeconds, "The minimun number of seconds to wait before hopping. Only applicable to hopping")
	flags.UintVar(&hopJitter, "j", hopJitter, "A random range to wait (on top of the minimum) seconds before hopping. Only applicable to hopping")
	flags.StringVar(&proxyPort, "p", proxyPort, "The port for the local proxy")
	flags.StringVar(&proxyHost, "i", proxyHost, "The host for the local proxy")
	err := flags.Parse(os.Args[1:])
	if err != nil {
		logger.Fatalf("error parsing flags: %v", err)
	}
	// Configure logging.
	if verbose {
		debug.SetOutput(os.Stderr)
	}

	obfs4RemotesList := strings.Split(obfs4Remotes, ",")
	obfs4CertsList := strings.Split(obfs4Certs, ",")

	if len(obfs4CertsList) != len(obfs4RemotesList) {
		logger.Fatalf("number of obfs4 remotes must match number of obfs4 certs")
	}

	if (maxHopPort - minHopPort) < portCount {
		logger.Fatalf("configured port hopping boundaries (range of %d ports) cannot be smaller than port count %d.", (maxHopPort - minHopPort), portCount)
	}

	proxyAddr := net.JoinHostPort(proxyHost, proxyPort)

	logger.Printf("proxyAddr: %+v", proxyAddr)

	// Setup graceful shutdown.
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)

	kcpConfig := obfsvpn.KCPConfig{
		Enabled:            kcp,
		SendWindowSize:     kcpSendWindowSize,
		ReceiveWindowSize:  kcpReceiveWindowSize,
		ReadBuffer:         kcpReadBuffer,
		WriteBuffer:        kcpWriteBuffer,
		NoDelay:            kcpNoDelay,
		Interval:           kcpInterval,
		Resend:             kcpResend,
		DisableFlowControl: kcpDisableFlowControl,
		MTU:                kcpMTU,
	}

	quicConfig := obfsvpn.QUICConfig{
		Enabled: quic,
	}

	var config client.Config

	if hoppingPT {
		config = client.Config{
			ProxyAddr:  proxyAddr,
			KCPConfig:  kcpConfig,
			QUICConfig: quicConfig,
			HoppingConfig: client.HoppingConfig{
				Enabled:       true,
				Remotes:       obfs4RemotesList,
				Obfs4Certs:    obfs4CertsList,
				PortSeed:      portSeed,
				PortCount:     portCount,
				MinHopPort:    minHopPort,
				MaxHopPort:    maxHopPort,
				MinHopSeconds: minHopSeconds,
				HopJitter:     hopJitter,
			},
		}
	} else {
		config = client.Config{
			ProxyAddr:     proxyAddr,
			KCPConfig:     kcpConfig,
			QUICConfig:    quicConfig,
			Obfs4Cert:     obfs4CertsList[0],
			RemoteIP:      obfs4RemotesList[0],
			RemotePort:    obfs4RemotePort,
			HoppingConfig: client.HoppingConfig{Enabled: false},
		}

	}
	c := client.NewClient(ctx, stop, config)
	if _, err = c.Start(); err != nil {
		log.Fatal(err)
	}
}
