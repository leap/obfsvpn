#!/bin/bash

if [[ "$LIVE_TEST" == "1" ]]; then exit 0; fi
curl -s $OBFSVPN_SERVER_HOST:9090/bridge | jq > bridge.json
cat bridge.json
if [[ `jq .type bridge.json` == "\"obfs4\"" ]]; then echo "successfully fetched bridge info"; else echo "parsing failure"; exit 1; fi;