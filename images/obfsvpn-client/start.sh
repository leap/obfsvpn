#!/bin/bash

set -eo pipefail

mkdir -p /dev/net

if ! [ -c /dev/net/tun ]; then
    echo "$(date) Creating tun/tap device."
    mknod /dev/net/tun c 10 200
fi

# Choose correct OpenVPN config
CONFIG="/vpn/client.ovpn"
ROUTE=""
if [[ "$LIVE_TEST" == "1" ]]; then
    CONFIG="/livetest/riseup.ovpn"
    ROUTE="--route $OBFS4_SERVER_HOST1 255.255.255.255 net_gateway"
fi
echo ">>> LIVE TEST: ${LIVE_TEST}"

# Choose outer transport protocol
FLAG_KCP=""
if [[ "$KCP" == "1" ]]; then
    FLAG_KCP="-kcp"
    echo "Using KCP on the wire"
elif [[ "$QUIC" == "1" ]]; then
    echo "Using QUIC on the wire"
    FLAG_QUIC="-quic"
fi

if [[ "$HOP_PT" == "1" ]]; then
    echo "Starting obfsvpn-client in hopping mode"
    # set a couple of optional hopping flags
    FLAG_MIN_HOP_PORT=""
    if [[ -n $OBFSVPN_MIN_HOP_PORT ]]; then
        FLAG_MIN_HOP_PORT="-min-port $OBFSVPN_MIN_HOP_PORT"
        echo "Setting Min hopping port to $OBFSVPN_MIN_HOP_PORT"
    fi

    FLAG_MAX_HOP_PORT=""
    if [[ -n $OBFSVPN_MAX_HOP_PORT ]]; then
        FLAG_MAX_HOP_PORT="-max-port $OBFSVPN_MAX_HOP_PORT"
        echo "Setting max hopping port to $OBFSVPN_MAX_HOP_PORT"
    fi

    FLAG_SEED=""
    if [[ -n $OBFSVPN_SEED ]]; then
        FLAG_SEED="-ps $OBFSVPN_SEED"
        echo "Setting hopping port seed to $OBFSVPN_SEED"
    fi

    FLAG_PORT_COUNT=""
    if [[ -n $OBFSVPN_PORT_COUNT ]]; then
        FLAG_PORT_COUNT="-pc $OBFSVPN_PORT_COUNT"
        echo "Setting hopping port count to $OBFSVPN_PORT_COUNT"
    fi
    /usr/bin/obfsvpn-client $FLAG_KCP $FLAG_QUIC -h -c "$OBFS4_CERT1,$OBFS4_CERT1" -r "$OBFS4_SERVER_HOST1,$OBFS4_SERVER_HOST2" \
      -m "$MIN_HOP_SECONDS" -j "$HOP_JITTER" $FLAG_MAX_HOP_PORT $FLAG_MIN_HOP_PORT $FLAG_SEED $FLAG_PORT_COUNT &

    # start openvpn in udp
    # set connect-retry low to help facilitate integration test
    openvpn --config $CONFIG --connect-retry 1 --remote 127.0.0.1 8080 $ROUTE

else
    /usr/bin/obfsvpn-client $FLAG_KCP $FLAG_QUIC -c "$OBFS4_CERT1" -r "$OBFS4_SERVER_HOST1" -rp "$OBFS4_PORT" -v &

    # start openvpn in udp
    # set connect-retry low to help facilitate integration test
    openvpn --config $CONFIG --connect-retry 1 --remote 127.0.0.1 8080 $ROUTE
fi

exit $?

