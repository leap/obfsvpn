#!/bin/bash

set -eo pipefail

echo ">>>> LIVE_TEST: ${LIVE_TEST}"
if [[ ${LIVE_TEST} == "1" ]]; then
  echo "Live test. Skipping openvpn server startup."
  exit 0
fi;


source ./functions.sh

mkdir -p /dev/net

if ! [ -c /dev/net/tun ]; then
    echo "$(datef) Creating tun/tap device."
    mknod /dev/net/tun c 10 200
fi

cd "$APP_PERSIST_DIR"

LOCKFILE=.gen

# Regenerate certs only on the first start
if ! [ -f $LOCKFILE ]; then
    IS_INITIAL="1"

    /usr/share/easy-rsa/easyrsa init-pki

    # DH parameters of size 2048 created at APP_PERSIST_DIR/pki
    /usr/share/easy-rsa/easyrsa gen-dh

    /usr/share/easy-rsa/easyrsa build-ca nopass << EOF

EOF
    # CA creation complete and you may now import and sign cert requests.
    # Your new CA certificate file for publishing is at:
    # /opt/Dockovpn_data/pki/ca.crt

    /usr/share/easy-rsa/easyrsa gen-req MyReq nopass << EOF2

EOF2
    # Keypair and certificate request completed. Your files are:
    # req: /opt/Dockovpn_data/pki/reqs/MyReq.req
    # key: /opt/Dockovpn_data/pki/private/MyReq.key

    /usr/share/easy-rsa/easyrsa sign-req server MyReq << EOF3
yes
EOF3
    # Certificate created at: /opt/Dockovpn_data/pki/issued/MyReq.crt

    openvpn --genkey secret ta.key << EOF4
yes
EOF4

    touch $LOCKFILE
fi

# Copy server keys, dh file and certificates
cp pki/dh.pem pki/ca.crt pki/issued/MyReq.crt pki/private/MyReq.key ta.key /etc/openvpn

# Update port and tun mtu in /etc/openvpn/server.conf
sed -i -e "s/__PORT__/${PORT}/g" \
  -e  "s/__TUN_MTU__/${TUN_MTU}/g" \
  /etc/openvpn/server.conf

# Need to feed key password
openvpn --config /etc/openvpn/server.conf &

# By some strange reason we need to do echo command to get to the next command
echo " "

# backoff for openvpn to start
sleep 20

# Generate client config
if [[ -n $IS_INITIAL ]]; then
  CLIENT_ID="$(head /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)"
  CLIENT_PATH="$APP_PERSIST_DIR/clients/$CLIENT_ID"
  createConfig "$CLIENT_PATH"

	FILE_NAME=client.ovpn
	FILE_PATH="$CLIENT_PATH/$FILE_NAME"
	sed -i -e "s/__TUN_MTU__/${TUN_MTU}/g" "${FILE_PATH}"

	echo "$(datef) Created ${FILE_PATH}."
  # dirty hack: copy client config to root of APP_PERSIST_DIR
  # for reusing in the obfsvpn-client container
  cat "$FILE_PATH"
  cp "${FILE_PATH}" "$APP_PERSIST_DIR/"
fi

# Allow UDP traffic on port 1194.
iptables -A INPUT -i eth0 -p udp -m state --state NEW,ESTABLISHED --dport 1194 -j ACCEPT
iptables -A OUTPUT -o eth0 -p udp -m state --state ESTABLISHED --sport 1194 -j ACCEPT

# Allow traffic on the TUN interface.
iptables -A INPUT -i tun0 -j ACCEPT
iptables -A FORWARD -i tun0 -j ACCEPT
iptables -A OUTPUT -o tun0 -j ACCEPT

# Allow forwarding traffic only from the VPN.
iptables -A FORWARD -i tun0 -o eth0 -s 10.8.0.0/24 -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

# Set mtu for tun0 device
ip link set mtu "$TUN_MTU" dev tun0

tail -f /dev/null

