# DEPRECATED

This README is no longer accurate/up to date. Instead of removing it entirely, I'd like to leave it as a signpost so that I can work with
someone on the team to bring it up to date/ensure that sufficient documentation exists

## Dockerized obfs4 and openvpn in a single container

This is based on the [dockovpn/docker-openvpn](https://github.com/dockovpn/docker-openvpn) repository, it adds the `obfsvpn-server`
binary and the required config files to start an instance of the server targetting the
`openvpn` server running at port 5540 (default)

A directory can be bind mounted to `/opt/Dockovpn_data` to persist the configurations of the
`openvpn` server and additionally to copy out the `client.ovpn` file created at `/opt/Dockovpn/client/<client-id>/client.ovpn`

The container recognises the following environment variables and can be used to override the
default values:

```
OBFS4_HOST = 0.0.0.0 (default) # This is the OBFS4 server endpoint
OBFS4_PORT = 4430
HOST_ADDR = localhost # openvpn gateway ip
PORT = 5540 # openvpn GW port
PROTO = tcp # openvpn proto
```

### To test locally

```
$ make obfsvpn-server-container
```

Then run the container using the following command:

```
$ docker run -it --rm --privileged -p 4430:4430/tcp leap/obfsvpn:0.1
```
