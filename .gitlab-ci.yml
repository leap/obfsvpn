---
include:
  - project: 'leap/container-platform/glue'
    file: '/.gitlab-ci.yml'


# Combined with the above include statement, these will be for building the obfsvpn server image
variables:
  DOCKERFILE: ./images/obfsvpn/Dockerfile
  IMAGE_TAG: $CI_REGISTRY_IMAGE:server-$CI_COMMIT_REF_SLUG
  RELEASE_TAG: $CI_REGISTRY_IMAGE:server-latest
  PREVIOUS_RELEASE_TAG: $CI_REGISTRY_IMAGE:server-previous

stages:
  - test
  - validate
  - integration-test
  - build
  - release

test:
  image: golang:1.22.2-alpine
  stage: test
  script:
    - apk add build-base
    - apk add git
    - go get ./... && go test -v ./...
  tags:
    - linux

validate:
  image: golang:1.22.2-alpine
  stage: test
  script: |
      apk add build-base git jq curl
      go version
      go env

      go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.60.1

      export PATH=$(go env GOPATH)/bin:$PATH

      go get ./... && go test -v ./...

      golangci-lint run --path-prefix=./ --timeout=5m

      ./scripts/check-obfs4-ver.sh

      go mod tidy
      git diff --exit-code -- go.mod go.sum

integration-test-default:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh
  tags:
    - linux
    - docker-in-docker

integration-test-hopping:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh hop
  tags:
    - linux
    - docker-in-docker

integration-test-kcp:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh kcp
  tags:
    - linux
    - docker-in-docker

integration-test-hopping-kcp:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh hop-kcp
  tags:
    - linux
    - docker-in-docker

integration-test-quic:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh quic
  tags:
    - linux
    - docker-in-docker

integration-test-hopping-quic:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - /bin/bash ./scripts/integration-test.sh hop-quic
  tags:
    - linux
    - docker-in-docker

integration-test-kcp-live-gw:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - apk add curl
    - /bin/bash ./scripts/integration-test.sh kcp-live-gw
  tags:
    - linux
    - docker-in-docker
  # allow failure since this test runs against a deployed gateway,
  # so occasional network failures are expected
  allow_failure: true
  artifacts:
    when: always
    expire_in: 3d
    paths:
    - integration_test.log

integration-test-kcp-live-bridge:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - apk add curl
    - /bin/bash ./scripts/integration-test.sh kcp-live
  tags:
    - linux
    - docker-in-docker
  # allow failure since this test runs against a deployed gateway,
  # so occasional network failures are expected
  allow_failure: true
  artifacts:
    when: always
    expire_in: 3d
    paths:
    - integration_test.log

integration-test-hopping-kcp-live-bridge:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - apk add curl
    - /bin/bash ./scripts/integration-test.sh hop-kcp-live
  tags:
    - linux
    - docker-in-docker
  # allow failure since this test runs against a deployed gateway,
  # so occasional network failures are expected
  allow_failure: true
  artifacts:
    when: always
    expire_in: 3d
    paths:
    - integration_test.log

integration-test-default-live-bridge:
  image: docker:dind
  stage: test
  services:
    - name: docker:dind
      alias: docker
  script:
    - apk add --update bash
    - apk add curl
    - /bin/bash ./scripts/integration-test.sh default-live
  tags:
    - linux
    - docker-in-docker
  # allow failure since this test runs against a deployed gateway,
  # so occasional network failures are expected
  allow_failure: true
  artifacts:
    when: always
    expire_in: 3d
    paths:
    - integration_test.log

# We can build the client separately here
build-client:
  extends: build
  variables:
    DOCKERFILE: ./images/obfsvpn-client/Dockerfile
    IMAGE_TAG: $CI_REGISTRY_IMAGE:client-$CI_COMMIT_REF_SLUG

release-client:
  extends: release
  variables:
    DOCKERFILE: ./images/obfsvpn-client/Dockerfile
    IMAGE_TAG: $CI_REGISTRY_IMAGE:client-$CI_COMMIT_REF_SLUG
    RELEASE_TAG: $CI_REGISTRY_IMAGE:client-latest
    PREVIOUS_RELEASE_TAG: $CI_REGISTRY_IMAGE:client-previous
