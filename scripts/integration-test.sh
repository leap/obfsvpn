#!/bin/bash

mode=$1
logfile=integration_test.log

print_logs() {
    echo "Integration test stopped. Getting logs." | tee -a $logfile
    echo "---------------------------------------" | tee -a $logfile
    docker compose -p "$mode" logs | tee -a $logfile
    docker compose -p "$mode" down --rmi all -v
}

trap "exit" INT TERM
trap "print_logs" EXIT

if [[ -n $mode ]]; then echo "Running mode: $1"; fi

if [[ -z "$mode" ]]; then
  echo "Running default mode (obfs4 local integration test)"
  env_file=./.env
elif [[ "$mode" == "hop" ]]; then
  env_file=./.env.hopping
elif [[ "$mode" == "kcp" ]]; then
  env_file=./.env.kcp
elif [[ "$mode" == "hop-kcp" ]]; then
  env_file=./.env.hopping.kcp
elif [[ "$mode" == "quic" ]]; then
  env_file=./.env.quic
elif [[ "$mode" == "hop-quic" ]]; then
  env_file=./.env.hopping.quic
elif [[ "$mode" == "kcp-live-gw" ]]; then
  env_file=./.env.kcp.live.gw
  ./scripts/generate_livetest_credentials.sh
elif [[ "$mode" == "kcp-live" ]]; then
  env_file=./.env.kcp.live.bridge
  ./scripts/generate_livetest_credentials.sh
elif [[ "$mode" == "default-live" ]]; then
  env_file=./.env.1ive.bridge
  ./scripts/generate_livetest_credentials.sh
elif [[ "$mode" == "hop-kcp-live" ]]; then
  env_file=./.env.hoppingkcp.live.bridge
  ./scripts/generate_livetest_credentials.sh
else
  echo "invalid parameter $mode"
  exit 1
fi

docker compose -p "$mode" down --rmi all -v
docker compose -p "$mode" build --parallel --no-cache
docker compose -p "$mode" --env-file $env_file up -d --build

# need to wait for openvpn to generate configs
max_initial_retry=40
counter=0

# Testing bridged tunnel
until docker compose -p "$mode" --env-file $env_file exec client ping -c 3 -I tun0 8.8.8.8
do
   ((counter++))
   [[ counter -eq $max_initial_retry ]] && echo "Failed!" | tee -a $logfile && exit 1
   echo "Pinging in client container with config $(cat ${env_file} | grep KCP) and $(cat ${env_file} | grep HOP_PT) failed. Trying again. Try #$counter" | tee -a $logfile
   sleep 30
done

counter=0
max_reconnect_retry=10

if [[ "$LIVE_TEST" == "0" ]]; then
  echo "Testing reconnect"

  # We can't just restart the obfsvpn servers because of some very odd way that key material is being generated so we go down and up instead 🤷
  docker compose -p "$mode" --env-file $env_file down obfsvpn-1 obfsvpn-2
  docker compose -p "$mode" --env-file $env_file up -d obfsvpn-1 obfsvpn-2

  until docker compose -p "$mode" --env-file $env_file exec client ping -c 3 -I tun0 8.8.8.8
  do
     ((counter++))
     [[ counter -eq $max_reconnect_retry ]] && echo "Failed!" | tee -a $logfile && exit 1
     echo "Pinging in client container with config $(cat ${env_file} | grep KCP) and $(cat ${env_file} | grep HOP_PT) after reconnect failed. Trying again. Try #$counter" | tee -a $logfile
     sleep 10
  done

fi

docker compose -p "$mode" --env-file $env_file exec client ndt7-client -quiet | tee -a $logfile

# Testing bridge control panel
if [[ "$LIVE_TEST" == "0" ]]; then
  if [[ $(docker compose -p "$mode" logs | grep control-client | tail -n1 | cut -d "|" -f2 | xargs) == "parsing failure" ]]
  then
    echo "failed to parse from control panel" | tee -a $logfile
    exit 1
  else
    echo "control panel parsing succeeded" | tee -a $logfile
  fi
fi