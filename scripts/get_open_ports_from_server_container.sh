#!/bin/bash

# 
# Prints out the container:host port bindings for the obfsvpn server container running in Hopping KCP mode.
# Workaround used for hard-coding port bindings in docker-compose.yml. 
#
# You first need to start the obfsvpn server container in Hopping KCP mode.
# you can then run this script on the host and get the required port bindings.
# Afterwards stop the obfsvpn server container and hard-code the port bindings in docker-compose.yml.
# Then restart the container with docker compose up -d. 
#


SERVICE=$1
if [[ -z $SERVICE ]]; then
    echo -e """
    Missing service parameter from your docker-compose.yml.

    Example:
    ./scripts/get_open_ports_from_server_container.sh hop-kcp-live-obfsvpn-1
    """
    exit 1
fi

# list docker containers | get hopping kcp containers | get first one | get container ID | print logs of container | get lines showing the open ports | get only the ports | print out port binding
docker ps | grep $SERVICE | head -1  | cut -d " " -f1 | xargs -I {} docker logs {} 2>&1 | grep "listen on" | cut -d " " -f6 | cut -d ":" -f2 | xargs -I {} echo "      - \"{}:{}/udp\""
