#!/bin/bash

PROVIDER_API_URL="https://api.black.riseup.net:4430"
SCRIPT_DIR=$(dirname "$0")
OBFSVPN_CLIENT_DATA_DIR=$SCRIPT_DIR/../images/obfsvpn-client/data

cd $OBFSVPN_CLIENT_DATA_DIR

echo Getting credentials from $PROVIDER_API_URL/3/cert...
echo Used in obfsvpn-client docker container

curl -s --cacert riseup_ca.crt $PROVIDER_API_URL/3/cert > credentials.txt

cat credentials.txt | grep 'END RSA PRIVATE KEY' -B 100 > riseup_client.key
cat credentials.txt | grep 'BEGIN CERTIFICATE' -A 100 > riseup_client.crt
rm credentials.txt

echo "generated credentials:"
cat riseup_client.key
cat riseup_client.crt
