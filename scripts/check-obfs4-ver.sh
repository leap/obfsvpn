#!/bin/sh
# check yawning's HEAD
available=$(curl -s https://gitlab.com/api/v4/projects/10387781/repository/tags/ | jq ".[0]".name | cut -d "-" -f 2 | cut -d "\"" -f 1)
current=$(cat scripts/obfs4-version)

echo "current:" $current
echo "available:" $available

if [ "$available" = "$current" ]; then 
        exit 0
else
        echo "New version of upstream yawning/obfs4 available, should update deps."
        exit 2
fi;
