# Changelog

<a name="v1.3.0"></a>
## [v1.3.0](https://0xacab.org/leap/obfsvpn/compare/v1.2.0...v1.3.0) (2024-10-04)

### Feature

* Add QUIC protcol functionality

<a name="v1.2.0"></a>
## [v1.2.0](https://0xacab.org/leap/obfsvpn/compare/v1.1.0...v1.2.0) (2024-09-24)

### Feature

* Add ability to start obfsvpn server in TCP mode

### Fix

* Fix logging errrors for FFI clients
* Fix reconnect failures in obfsvpn clients

<a name="v1.1.0"></a>
## [v1.1.0](https://0xacab.org/leap/obfsvpn/compare/v1.0.0...v1.1.0) (2024-08-05)

### Feature

* Add additional KCP flags to obfsvpn server and client
* Add GetKCPStats function
* Change defaults to turbo mode

### Fix

* Properly pass kcp configs to server listener
* Fix KCP listener config
* Fix hop-kcp integration test, which was running as obfs4 hopping pt due to the checks in images/obfsvpn-client/start.sh . Small refactoring so that we require less services to be bootstrapped

* Always close clients UDP connection on localhost:8080 when stopping obfsvpn
* Always send state updates via EventLogger in case it is not nil
* fix method naming for newFFIClient()


<a name="v1.0.0"></a>
## [v1.0.0](https://0xacab.org/leap/obfsvpn/compare/v0.1.0...v1.0.0) (2024-05-14)

### Feature

* Unify hopping and regular API


### BREAKING CHANGE

This changes the manner in which bitmask/openvpn clients need to connect and the way in which bitmask openvpn servers must run. Instead of the "regular"/non-hopping PT using openvpn in TCP mode, both the hopping and non-hopping PT configurations need to use UDP mode.

The openvpn client [must be run with a remote specified instead of a socks proxy](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#fdfa9935cf5ed31d7e1155d44f72dec9870e5d38_39_37) and 
the [config must specify udp for the proto](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#8e8031bf471f30d718591b3348308ae806cc7a0a_3_3)

The openvpn server [also needs to change the proto to udp](https://0xacab.org/leap/obfsvpn/-/commit/09b1497c152ea5d52cb44ad42174f3ac9528bce8#708b8cab47f3e851eae0805b34783382bc81e165_3_2)
