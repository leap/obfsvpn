package server

type BridgeServer interface {
	Start() error
	Stop()
}
