package server

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net"
	"strconv"

	"0xacab.org/leap/obfsvpn/obfsvpn"
)

// UDPServer is a obfsvpn server
type UDPServer struct {
	cfg    ServerConfig
	logger *log.Logger
	debug  *log.Logger
	ctx    context.Context
	stop   context.CancelFunc
}

// NewUDPServer returns a new UDPServer
func NewUDPServer(ctx context.Context, stop context.CancelFunc, cfg ServerConfig, logger, debug *log.Logger) BridgeServer {
	return &UDPServer{
		ctx:    ctx,
		stop:   stop,
		cfg:    cfg,
		debug:  debug,
		logger: logger,
	}
}

// Start starts the obfsvpn server
func (s *UDPServer) Start() error {
	listenConfig, err := obfsvpn.NewListenConfig(
		s.cfg.Obfs4Config.NodeID, s.cfg.Obfs4Config.PrivateKey, s.cfg.Obfs4Config.PublicKey,
		s.cfg.Obfs4Config.DRBGSeed,
		s.cfg.StateDir,
		s.cfg.KCPConfig,
		s.cfg.QUICConfig,
	)
	if err != nil {
		return fmt.Errorf("error creating listener from config: %w", err)
	}

	// We want a non-crypto RNG so that we can share a seed
	// #nosec G404
	r := rand.New(rand.NewSource(s.cfg.PortSeed))

	s.debug.Printf("DEBUG: %v", listenConfig)

	var listeners []net.Listener

	if s.cfg.HoppingEnabled {
		listeners = make([]net.Listener, s.cfg.PortCount)
		portHopRange := int(s.cfg.MaxHopPort - s.cfg.MinHopPort)
		for i := 0; i < int(s.cfg.PortCount); i++ {
			portOffset := r.Intn(portHopRange)
			addr := net.JoinHostPort(s.cfg.Obfs4ListenIP, fmt.Sprint(portOffset+int(s.cfg.MinHopPort)))
			listeners[i], err = listenConfig.Listen(s.ctx, addr)

			if err != nil {
				s.logger.Printf("Error binding to %s: %v", addr, err)
			}
		}
	} else {
		listenAddr := net.JoinHostPort(s.cfg.Obfs4ListenIP, strconv.Itoa(s.cfg.Obfs4ListenPort))
		listener, err := listenConfig.Listen(s.ctx, listenAddr)

		if err != nil {
			s.logger.Printf("Error binding to %s: %v", listenAddr, err)
		}

		listeners = []net.Listener{listener}
	}

	for _, ln := range listeners {
		go s.acceptLoop(ln)
	}

	<-s.ctx.Done()
	// Stop releases the signal handling and falls back to the default behavior,
	// so sending another interrupt will immediately terminate.
	s.stop()
	s.logger.Printf("shutting down…")
	for _, ln := range listeners {
		err := ln.Close()
		if err != nil {
			s.logger.Printf("error closing listener: %v", err)
		}
	}
	return nil
}

func (s *UDPServer) Stop() {
	s.logger.Println("Stopping server")
	s.stop()
}

func (s *UDPServer) acceptLoop(ln net.Listener) {
	s.logger.Printf("Listening on %s…", ln.Addr())

	for {
		conn, err := ln.Accept()
		if err != nil {
			s.debug.Printf("error accepting connection: %v", err)
			continue
		}
		s.debug.Printf("accepted connection from %v", conn.RemoteAddr())

		udpRemote, err := net.ResolveUDPAddr("udp", s.cfg.OpenvpnAddr)
		if err != nil {
			s.logger.Printf("Error binding to %s: %v", s.cfg.OpenvpnAddr, err)
		}

		udpConn, err := net.DialUDP("udp", nil, udpRemote)
		if err != nil {
			s.logger.Printf("error dialing to %s: %v", udpRemote, err)
		}

		go readTCPWriteUDP(conn, udpConn, s.debug, s.logger)

		go readUDPWriteTCP(conn, udpConn, s.debug, s.logger)
	}
}

func readTCPWriteUDP(tcpConn net.Conn, udpConn *net.UDPConn, debug, logger *log.Logger) {
	lengthBuffer := make([]byte, 2)
	datagramBuffer := make([]byte, obfsvpn.MaxUDPLen)
	for {
		udpBuffer, err := obfsvpn.ReadTCPFrameUDP(tcpConn, datagramBuffer, lengthBuffer)
		if err != nil {
			debug.Printf("Reading/framing error: %v\n", err)
			break
		}

		_, err = udpConn.Write(udpBuffer)
		if err != nil {
			debug.Printf("Write err  %v\n", err)
			break
		}
	}
}

func readUDPWriteTCP(tcpConn net.Conn, udpConn *net.UDPConn, debug, logger *log.Logger) {
	datagramBuffer := make([]byte, obfsvpn.MaxUDPLen)
	for {
		tcpBuffer, _, err := obfsvpn.ReadUDPFrameTCP(udpConn, datagramBuffer)
		if err != nil {
			debug.Printf("Reading/framing error: %v", err)
			break
		}
		_, err = tcpConn.Write(tcpBuffer)
		if err != nil {
			debug.Printf("Write err on %v to %v: %v\n", tcpConn.LocalAddr(), tcpConn.RemoteAddr(), err)
			break
		}
	}
}
