package client

import (
	"encoding/json"
	"testing"
)

// This json was generated from the java model classes
var expected = `{"hopping_config":{"enabled":false,"hop_jitter":10,"min_hop_seconds":10,"obfs4_certs":["XXXXYYYYZZZZ"],"port_count":0,"port_seed":0,"proxy_addr":"127.0.0.1:8080","remotes":["37.218.4.12"]},"kcp_config":{"mtu":1400,"disable_flow_control":true,"enabled":true,"interval":10,"no_delay":true,"read_buffer":16777216,"receive_window_size":65535,"resend":2,"send_window_size":65535,"write_buffer":16777216},"obfs4_cert":"XXXYYYYZZZZZ","proxy_addr":"127.0.0.1:8080","remote_ip":"37.218.4.12","remote_port":"4431"}`

func TestJsonFormattingEnsureFFIInterop(t *testing.T) {
	config := Config{}
	err := json.Unmarshal([]byte(expected), &config)
	if err != nil {
		t.Error("unexpected marshalling error: ", err)
	}

	if !config.KCPConfig.DisableFlowControl {
		t.Error("DisableFlowControl should be false")
	}

	if config.KCPConfig.MTU != 1400 {
		t.Error("Unexpected MTU")
	}
}
